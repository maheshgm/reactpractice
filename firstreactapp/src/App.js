
import './App.css';
import React, { useState } from 'react';
// import FunPropsExpm from './components/props/funPropsExpm';
// import ClassPropsExpm from './components/props/classPropsExpm';
import HookCounterOne from './components/Hooks/useEffect/HookCounterOne';
import ClassMouseMove from './components/Hooks/EventListener/ClassMouseMove';
import HookMouseMove from './components/Hooks/EventListener/HookMouseMove';
import ClassIntervalCounter from './components/Hooks/useEffect/ClassIntervalCounter';
import HookIntervalCounter from './components/Hooks/useEffect/HookIntervalCounter';
import FetchDate from './components/Hooks/Axios/FetchDate';
import ComponentC from './components/Hooks/useContext/ComponentC';
import WithContextHook from './components/Hooks/useContext/WithContextHook';

export const userContext = React.createContext();
export const channelContext = React.createContext();


function App() {
  // functional component props example
  // const myMethod = (currDate) => {
  //   console.log("In function component some one is calling me at : " + currDate);
  // }

  // const clsMethod = (currDate) => {
  //   console.log("In class component some one is calling me at : " + currDate);
  // }
  // const [display, setDisplay] = useState(true);

  // const displayMouseMove = (value) => {
  //   setDisplay(value);
  // }
  return (
    <div className="App">
      <h3>My Firstreactapp</h3>
      <userContext.Provider value={'Mahesh'}><channelContext.Provider value="it's Me Maheeee"><ComponentC /></channelContext.Provider> </userContext.Provider>


      <WithContextHook />

      {/* Fetching Date from jsonplaceholder.typicode.com */}
      {/* <FetchDate /> */}
      {/*  <HookIntervalCounter />
      <ClassIntervalCounter /> */}



      {/* <ClassMouseMove />
      <HookCounterOne /> */}
      {/* {display && <HookMouseMove />}
      <button onClick={() => {
        displayMouseMove(false)
      }}>display</button> */}

      {/* passing function as props to function component */}
      {/* <p> passing function as props to function component</p>
      <FunPropsExpm name="Mahee" method={myMethod} /> */}

      {/* passing function as props to Class component */}
      {/* <p> passing function as props to Class component </p>
      <ClassPropsExpm name="Mahesh " clsMethod={clsMethod} ><div>childrenPropertyName</div></ClassPropsExpm> */}

    </div >
  );
}

export default App;
