import React, { Component } from 'react';

class ClassPropsExpm extends Component {
    constructor(props) {
        super(props);
        console.log("constructor")
    }


    render() {

        //Distructuring the props object
        const { name, clsMethod, children } = this.props;
        return (
            <div>
                <h3>Class Properties</h3>
                {children}
                {this.props.children}
                {/* <button onClick={() => this.props.clsMethod(new Date())}>{this.props.name}Click me</button> */}
                <button onClick={() => clsMethod(new Date())}>{name}Click me</button>

            </div>
        );
    }
}

export default ClassPropsExpm;
