import React from 'react';

const FunPropsExpm = (props) => {
    //distructure the props from
    const { name, method } = props
    // console.log(method(new Date()));
    return (
        <div>
            <h3> This is FunPropsExpm</h3>
            <button onClick={() => { method(new Date()) }}>{name} Click Me</button>

        </div>
    );
}

export default FunPropsExpm;
