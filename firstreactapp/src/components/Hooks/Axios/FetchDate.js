import axios from 'axios';
import React, { useState, useEffect } from 'react'

function FetchDate() {
    const [posts, setPosts] = useState([]);
    //getData By Id
    const [id, setId] = useState(1);
    const [fetchId, setFetchId] = useState(1);

    const fetchingId = () => {
        setFetchId(id)
    }

    useEffect(() => {
        axios.get(`https://jsonplaceholder.typicode.com/posts/${fetchId}`).then(
            res => {
                console.log(res);
                setPosts(res.data)
            }
        ).catch(error => {
            console.log(error);
        })
        return () => {

        };
    }, [fetchId]);

    // useEffect(() => {
    //     console.log(posts[0])
    // }, [posts])


    return (
        <div>
            <div>
                <input type="text" value={id} onChange={(e) => { setId(e.target.value) }} />
                <button onClick={fetchingId}>fetchData</button>
            </div>
            <div>{posts.title}
            </div>

        </div >
    )
}

export default FetchDate
