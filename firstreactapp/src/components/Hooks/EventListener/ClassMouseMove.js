import React, { Component } from 'react';
import PropTypes from 'prop-types';


class ClassMouseMove extends Component {
    constructor(props) {
        super(props);
        this.state = {
            x: 0,
            y: 0
        }
    }
    logMousePosition = e => {

        this.setState({ x: e.clientX, y: e.clientY })
    }
    componentDidMount() {
        window.addEventListener('mousemove', this.logMousePosition)
    }

    render() {
        return (
            <div>
                ClassMouseMove X axis: {this.state.x}  Y axis: {this.state.y}
            </div>
        );
    }
}


ClassMouseMove.propTypes = {

};


export default ClassMouseMove;
