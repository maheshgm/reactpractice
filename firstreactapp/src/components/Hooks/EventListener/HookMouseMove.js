import React, { useState, useEffect } from 'react'

function HookMouseMove() {
    let initialState = 0;
    const [x, setX] = useState(initialState);
    const [y, setY] = useState(initialState);
    const mousePosition = (e) => {
        console.log('mousePosition called...');
        setX(e.x);
        setY(e.y);
    }

    useEffect((p) => {
        //mounting code
        console.log('useEffect Called... : ' + p)
        window.addEventListener('mousemove', mousePosition)
        return () => {
            //unmounting code
            console.log('useEffect unmounting ')
            window.removeEventListener('mousemove', mousePosition)
        };
    },
        //when should render
        []);

    return (
        <div>
            X - {x}  Y  - {y}
        </div>
    )
}

export default HookMouseMove
