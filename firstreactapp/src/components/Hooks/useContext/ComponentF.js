import React, { useContext } from 'react'
import { channelContext, userContext } from '../../../App';
import { userNameContext, passwordContext } from './WithContextHook'


function ComponentF() {
    const userName = useContext(userNameContext);
    const password = useContext(passwordContext);
    return (
        <div>
            ComponentF
            <userContext.Consumer>
                {user => {
                    return (<channelContext.Consumer>
                        {channel => {
                            return (<div>userContext Value {user},{channel}</div>)
                        }}
                    </channelContext.Consumer>)
                }}
            </userContext.Consumer>

            ===================================
            <p>{userName}</p>
            <p>{password}</p>

        </div>
    )
}

export default ComponentF
