// Context provides a way to pass data through the component tree without having to pass props down manually at every level


//React Context is a way to manage state globally. It can be used together with the useState Hook to share state between deeply nested components more easily than with useState alone.

import React from 'react'
import ComponentC from './ComponentC';

export const userNameContext = React.createContext();
export const passwordContext = React.createContext();

function WithContextHook() {
    return (
        <div>
            <userNameContext.Provider value={'Mahesh'}>
                <passwordContext.Provider value={'Maheeeeee'}>
                    <ComponentC />
                </passwordContext.Provider>
            </userNameContext.Provider>

        </div>
    )
}

export default WithContextHook
