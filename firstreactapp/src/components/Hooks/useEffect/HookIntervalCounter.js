import React, { useState, useEffect } from 'react'

function HookIntervalCounter() {
    const [count, setCount] = useState(0);

    const tickTick = () => {
        console.log('HookIntervalCounter');
        setCount(count + 1);
    }
    useEffect(() => {
        const interval = setInterval(tickTick, 1000);

        return () => {
            clearInterval(interval)
            console.log("jhbj  " + clearInterval(interval) + " " + clearInterval(tickTick) + " " + interval)
        };
    });
    return (
        <div>
            function Component {count}
        </div>
    )
}

export default HookIntervalCounter
