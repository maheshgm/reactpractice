import React, { Component } from 'react'

export class ClassIntervalCounter extends Component {
    state = {
        count: 0,
    }
    tickTick = () => {
        this.setState({
            count: this.state.count + 1,
        })
        //console.log('tick');
    }
    componentDidMount() {
        console.log("componentDidMount...")
        setInterval(this.tickTick, 1000);
    }
    componentWillUnmount() {
        console.log("componentWillUnmount..")
        clearInterval(this.tickTick);
    }

    render() {
        return (
            <div>
                Class Component {this.state.count}
            </div>
        )
    }
}

export default ClassIntervalCounter
