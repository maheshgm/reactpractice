import React, { useState, useEffect } from 'react';

const HookCounterOne = () => {
    let initialState = 0;
    const [count, setCount] = useState(initialState);
    // const Increment = () => {
    //     setCount(count + 1);
    // }
    // const Decrement = () => {
    //     setCount(count - 1);
    // }
    useEffect(() => {
        document.title = `You Clicked ${count} Times`;
        return () => {
            console.log(count);
        };
    });
    return (
        <div>

            {/* <button onClick={Increment} > Increment {count} times</button>
            <button onClick={Decrement} > Decrement {count} times</button> */}
            <button onClick={() => setCount(count + 1)} > Clicked {count} times</button>
            <button onClick={() => console.log("Clicked here")} > Clicked</button>

        </div>
    );
}

export default HookCounterOne;
